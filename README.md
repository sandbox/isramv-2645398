# Drupal 8 Materialize theme.

Implementation of materializecss.com into Drupal 8

## Demo here: http://dev-d8-materialize.pantheon.io/

Once enabled configure blocks to the new regions.

## Compatible with materializecss version: 0.97.7

Install:

Go to your theme folder /d8_materialize/ and then `npm install --save`, this step is required to fetch the acutal materialize library.

